# Workflow d'exemple pour la construction et le déploiement d'un site Nuxt sur GitHub Pages
#
# Pour commencer avec Nuxt, voir : https://nuxtjs.org/docs/get-started/installation
#
name: Deploy Nuxt site to Pages

on:
  # S'exécute sur les push ciblant la branche par défaut
  push:
    branches: ["stable"]

  # Permet d'exécuter ce workflow manuellement depuis l'onglet Actions
  workflow_dispatch:

env:
  NODE_VERSION: 21

# Définit les autorisations du jeton GITHUB_TOKEN pour permettre le déploiement sur GitHub Pages
permissions:
  contents: read
  pages: write
  id-token: write

# Autorise un seul déploiement concurrent, en sautant les exécutions en file d'attente entre l'exécution en cours et la dernière en file d'attente.
# Cependant, NE PAS annuler les exécutions en cours, car nous voulons permettre à ces déploiements de production de se terminer.
concurrency:
  group: "pages"
  cancel-in-progress: false

jobs:
  # === TESTS E2E === #
  cypress:
    # Utilisation d'une image Ubuntu
    runs-on: ubuntu-latest
    # Etapes du workflow
    steps:
      # Récupération du code de la branche
      - name: Checkout
        uses: actions/checkout@v4

      # Lancement du job Cypress
      - name: Cypress run
        uses: cypress-io/github-action@v6
        with:
          build: npm run build
          start: npm start
          browser: edge

  # === TESTS UNITAIRES === #
  vitest:
    runs-on: ubuntu-latest

    permissions:
      # Required to check out the code
      contents: read
      # Required to put a comment into the pull-request
      pull-requests: write

    steps:
      - uses: actions/checkout@v4
      - name: 'Install Node'
        uses: actions/setup-node@v4
        with:
          node-version: '20.x'
      - name: 'Install Deps'
        run: npm install
      - name: 'Test'
        run: npx vitest --coverage.enabled true
      - name: 'Report Coverage'
        # Set if: always() to also generate the report if tests are failing
        # Only works if you set `reportOnFailure: true` in your vite config as specified above
        if: always()
        uses: davelosert/vitest-coverage-report-action@v2



  # === BUILD DE L'APPLICATION === #
  build:
    # Jobs requis pour le workflow
    needs: [ "vitest", "cypress" ]
    # Utilisation d'une image Ubuntu
    runs-on: ubuntu-latest
    # Etapes du workflow
    steps:
      # Récupération du code de la branche
      - name: Checkout
        uses: actions/checkout@v4

      # Détection du gestionnaire de package
      - name: Detect package manager
        id: detect-package-manager
        run: |
          if [ -f "${{ github.workspace }}/yarn.lock" ]; then
            echo "manager=yarn" >> $GITHUB_OUTPUT
            echo "command=install" >> $GITHUB_OUTPUT
            exit 0
          elif [ -f "${{ github.workspace }}/package.json" ]; then
            echo "manager=npm" >> $GITHUB_OUTPUT
            echo "command=ci" >> $GITHUB_OUTPUT
            exit 0
          else
            echo "Unable to determine package manager"
            exit 1
          fi
      # Installation de node
      - name: Setup Node
        uses: actions/setup-node@v4
        with:
          node-version: ${{env.NODE_VERSION}}
          cache: npm

      # Configuration des pages Github
      - name: Setup Pages
        uses: actions/configure-pages@v4
        with:
          # Automatically inject router.base in your Nuxt configuration file and set
          # target to static (https://nuxtjs.org/docs/configuration-glossary/configuration-target/).
          # You may remove this line if you want to manage the configuration yourself.
          static_site_generator: nuxt

      # Installation des dépendances
      - name: Install dependencies
        run: ${{ steps.detect-package-manager.outputs.manager }} ${{ steps.detect-package-manager.outputs.command }}

      # Build de l'application
      - name: Static HTML export with Nuxt
        run: ${{ steps.detect-package-manager.outputs.manager }} run generate

      # Upload du résultat du build
      - name: Upload artifact
        uses: actions/upload-pages-artifact@v3
        with:
          path: ./dist

  # === DÉPLOIEMENT DE L'APPLICATION === #
  deploy:
    # Jobs requis pour le workflow
    needs: build
    # Utilisation d'une image Ubuntu
    runs-on: ubuntu-latest
    # Environnement
    environment:
      name: github-pages
      url: ${{ steps.deployment.outputs.page_url }}
    # Etapes du workflow
    steps:
      - name: Deploy to GitHub Pages
        id: deployment
        uses: actions/deploy-pages@v4
